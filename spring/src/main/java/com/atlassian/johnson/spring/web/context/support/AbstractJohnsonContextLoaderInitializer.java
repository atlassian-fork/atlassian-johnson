package com.atlassian.johnson.spring.web.context.support;

import com.atlassian.johnson.context.JohnsonContextListener;
import com.atlassian.johnson.spring.web.context.JohnsonContextLoaderListener;
import org.springframework.web.context.AbstractContextLoaderInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * Extends Spring's {@code AbstractDispatcherServletInitializer} to use Johnson-aware components.
 * <ul>
 * <li>A {@link JohnsonContextListener} will be registered <i>before</i> any other listeners that are registered
 * by this initializer</li>
 * <li>A {@link JohnsonContextLoaderListener} will be used to initialize the {@link #createRootApplicationContext()
 * root ApplicationContext}</li>
 * </ul>
 * <p>
 * In addition to using Johnson-aware components by default, this base class allows derived initializers to override
 * {@link #createContextLoaderListener(WebApplicationContext) the ContextLoaderListener} used. This is intended to
 * allow for application-specific handling on top of the Johnson-aware handling.
 *
 * @since 3.0
 */
public abstract class AbstractJohnsonContextLoaderInitializer extends AbstractContextLoaderInitializer {

    /**
     * Creates a {@link JohnsonContextLoaderListener} which will initialize and terminate the provided
     * {@code WebApplicationContext}. This method is provided as a convenience for derived classes to
     * simplify replacing the listener used.
     *
     * @param context the {@code WebApplicationContext} to be initialized by the created listener
     * @return the listener to register with the {@code ServletContext}
     */
    protected ContextLoaderListener createContextLoaderListener(WebApplicationContext context) {
        return new JohnsonContextLoaderListener(context);
    }

    /**
     * {@link #registerJohnsonContextListener(ServletContext) Registers a} {@link JohnsonContextListener} and then
     * delegates to the superclass's {@code onStartup(ServletContext)} implementation.
     *
     * @param servletContext the {@code ServletContext} to initialize
     * @throws ServletException potentially thrown by the superclass {@code onStartup(ServletContext)} implementation
     */
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        registerJohnsonContextListener(servletContext);

        super.onStartup(servletContext);
    }

    /**
     * Overrides {@code AbstractContextLoaderInitializer}'s {@code registerContextLoaderListener} to register a
     * {@link #createContextLoaderListener(WebApplicationContext) JohnsonContextLoaderListener} instead of the
     * standard Spring {@code ContextLoaderListener}.
     *
     * @param servletContext the {@code ServletContext} to register the {@link JohnsonContextLoaderListener} in
     */
    @Override
    protected void registerContextLoaderListener(ServletContext servletContext) {
        WebApplicationContext context = createRootApplicationContext();
        if (context == null) {
            logger.debug("No ContextLoaderListener registered, as createRootApplicationContext() did not return an application context");
        } else {
            servletContext.addListener(createContextLoaderListener(context));
        }
    }

    /**
     * Registers an {@link JohnsonContextListener} in in the provided {@code ServletContext}. This listener ensures
     * Johnson is initialized and terminated with the application.
     * <p>
     * Note: Even if this method is called multiple times, with its default implementation the listener will only be
     * added <i>once</i>.
     *
     * @param servletContext the {@code ServletContext} to register the {@link JohnsonContextListener} in
     * @see JohnsonContextListener#register(ServletContext)
     */
    protected void registerJohnsonContextListener(ServletContext servletContext) {
        JohnsonContextListener.register(servletContext);
    }
}
