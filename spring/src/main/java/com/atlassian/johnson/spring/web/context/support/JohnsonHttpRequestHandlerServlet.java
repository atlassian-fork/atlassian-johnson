package com.atlassian.johnson.spring.web.context.support;

import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.config.JohnsonConfig;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.spring.web.context.JohnsonContextLoaderListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.HttpRequestHandlerServlet;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.annotation.Nonnull;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;

/**
 * @since 2.1
 */
public class JohnsonHttpRequestHandlerServlet extends HttpRequestHandlerServlet {

    private static final Logger LOG = LoggerFactory.getLogger(JohnsonHttpRequestHandlerServlet.class);
    private static final Field TARGET;

    private final Object lock = new Object();

    private String contextAttribute;

    private volatile boolean uninitialised = true;

    static {
        TARGET = ReflectionUtils.findField(HttpRequestHandlerServlet.class, "target");
        ReflectionUtils.makeAccessible(TARGET);
    }

    public String getContextAttribute() {
        return contextAttribute;
    }

    @Override
    public void init() throws ServletException {
        applyParameters();

        //During startup, we'd like to initialise if possible because doing so here doesn't require locking. However,
        //if the WebApplicationContext was bypassed, attempting to initialise here will fail and may make the entire
        //server inaccessible. That would prevent accessing the Johnson page, so if the WebApplicationContext is not
        //available we bypass initialisation here.
        maybeInit();
    }

    public void setContextAttribute(String contextAttribute) {
        this.contextAttribute = contextAttribute;
    }

    protected void applyParameters() {
        ServletConfig config = getServletConfig();
        if (config != null) {
            setContextAttribute(config.getInitParameter("contextAttribute"));
        }
    }

    protected void sendRedirect(@Nonnull HttpServletResponse response) throws IOException {
        ServletContext servletContext = getServletContext();
        JohnsonConfig config = Johnson.getConfig(servletContext);

        LOG.warn("HttpRequestHandlerServlet [{}] cannot be initialised to service an incoming " +
                "request; redirecting to {}", getServletName(), config.getErrorPath());
        response.sendRedirect(servletContext.getContextPath() + config.getErrorPath());
    }

    @Override
    protected void service(@Nonnull HttpServletRequest request, @Nonnull HttpServletResponse response)
            throws ServletException, IOException {
        //When a request comes in, we're required to be initialised. If we're not, the only possible reason is that
        //the application is Johnsoned. That suggests we will not be able to initialise here, but we still try, on
        //the assumption that this URL was allowed through the Johnson filter and may, therefore, work. If so, the
        //request will be processed normally.
        if (uninitialised) {
            synchronized (lock) {
                if (uninitialised) {
                    if (!maybeInit()) {
                        //If we can't initialise at this point, redirect to the Johnson error page. The filter should
                        //not have allowed access to this URL.
                        sendRedirect(response);

                        return;
                    }
                }
            }
        }

        super.service(request, response);
    }

    /**
     * An enhanced version of {@code HttpRequestHandlerServlet.init()} which supports looking under a named
     * {@code ServletContext} attribute for the {@code WebApplicationContext} rather than only looking for
     * the root context.
     * <p>
     * Unfortunately, because {@code HttpRequestHandlerServlet.target} is {@code private} and has no mutator,
     * this method can only set it using Reflection.
     *
     * @throws ServletException if initialization cannot be performed
     */
    private void enhancedInit() throws ServletException {
        WebApplicationContext wac = findWebApplicationContext();

        ReflectionUtils.setField(TARGET, this, wac.getBean(getServletName(), HttpRequestHandler.class));
    }

    /**
     * Mimics the approach used by {@code DelegatingFilterProxy} to find the {@code WebApplicationContext}.
     *
     * @return the {@code WebApplicationContext}
     * @throws IllegalStateException if no {@code WebApplicationContext} can be found
     */
    private WebApplicationContext findWebApplicationContext() {
        String attribute = getContextAttribute();
        if (attribute == null) {
            return WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        }

        WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(getServletContext(), attribute);
        if (wac == null) {
            throw new IllegalStateException("No WebApplicationContext was found on the '" +
                    attribute + "' attribute on the ServletContext");
        }

        return wac;
    }

    private boolean maybeInit() throws ServletException {
        ServletContext servletContext = getServletContext();

        Object attribute = servletContext.getAttribute(JohnsonContextLoaderListener.ATTR_BYPASSED);
        //First, check to see if the WebApplicationContext was bypassed. If it was, it's possible, based on
        //configuration, that no event was added. However, we must bypass handler initialisation as well,
        //because no parent context will be available.
        if (Boolean.TRUE == attribute) //Fully bypassed, without even trying to start
        {
            LOG.error("Bypassing HttpRequestHandlerServlet [{}] initialisation; Spring initialisation was bypassed",
                    getServletName());
            return false;
        }
        //If WebApplicationContext initialisation wasn't bypassed, check to see if it failed. Handler initialisation
        //is guaranteed to fail if the primary WebApplicationContext failed, so we'll want to bypass it.
        if (attribute instanceof Event) {
            Event event = (Event) attribute;

            LOG.error("Bypassing HttpRequestHandlerServlet [{}] initialisation; Spring initialisation failed: {}",
                    getServletName(), event.getDesc());
            return false;
        }

        //If we make it here, the Spring WebApplicationContext should have started successfully. That means it's safe
        //to try and start this handler.
        try {
            enhancedInit();

            //No longer uninitialised, so future calls through this servlet should be serviced normally. Setting this
            //optimises the locking out of the service(...) method.
            uninitialised = false;
        } catch (Exception e) {
            LOG.error("HttpRequestHandlerServlet [" + getServletName() + "] could not be started", e);

            return false;
        }

        return true;
    }
}
