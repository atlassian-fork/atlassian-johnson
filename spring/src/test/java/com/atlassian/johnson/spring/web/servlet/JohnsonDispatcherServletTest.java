package com.atlassian.johnson.spring.web.servlet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockServletConfig;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.spy;

@PrepareForTest(JohnsonDispatcherServlet.class)
@RunWith(PowerMockRunner.class)
public class JohnsonDispatcherServletTest {

    @Test
    public void testInitSetsProperties() throws Exception {
        //DispatcherServlet.initServletBean() requires a lot of setup to pass, so it's vastly preferable to stub
        //it out. Unfortunately, it's also protected final. This uses PowerMock's @PrepareForTest/spy combination
        //to allow stubbing it anyway, letting this test focus on testing property setting
        JohnsonDispatcherServlet servlet = spy(new JohnsonDispatcherServlet());
        doNothing().when(servlet, "initServletBean");

        MockServletConfig config = new MockServletConfig();
        config.addInitParameter("servletContextAttributeName", "foo");

        servlet.init(config);

        assertEquals("foo", servlet.getServletContextAttributeName());
    }
}
