package com.atlassian.johnson.plugin.servlet.filter;

import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.plugin.servlet.filter.FilterDispatcherCondition;
import com.atlassian.plugin.servlet.filter.FilterLocation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class JohnsonServletFilterModuleContainerFilterTest {

    @Mock
    private FilterConfig config;
    @Mock
    private JohnsonEventContainer eventContainer;
    @InjectMocks
    private JohnsonServletFilterModuleContainerFilter filter;
    @Mock
    private FilterChain filterChain;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private ServletContext servletContext;

    @Before
    public void setup() throws ServletException {
        when(config.getInitParameter(eq("dispatcher"))).thenReturn(FilterDispatcherCondition.REQUEST.name());
        when(config.getInitParameter(eq("location"))).thenReturn(FilterLocation.AFTER_ENCODING.name());
        when(config.getServletContext()).thenReturn(servletContext);

        when(servletContext.getAttribute(eq(Johnson.ATTR_EVENT_CONTAINER))).thenReturn(eventContainer);

        filter.init(config);
    }

    /**
     * When there are no Johnson events present, verify that the normal {@code ServletFilterModuleContainerFilter}
     * logic is invoked. This is done by verifying that an attempt is made to get the {@code ServletModuleManager}.
     *
     * @throws Exception should not be thrown
     */
    @Test
    public void testDoFilter() throws Exception {
        filter.doFilter(request, response, filterChain);

        InOrder ordered = inOrder(config, eventContainer, filterChain, servletContext);
        ordered.verify(servletContext).getAttribute(eq(Johnson.ATTR_EVENT_CONTAINER));
        ordered.verify(eventContainer).hasEvents();
        ordered.verify(config).getServletContext();
        ordered.verify(servletContext).getAttribute(endsWith("servletModuleManager"));
        ordered.verify(filterChain).doFilter(same(request), same(response));
        ordered.verifyNoMoreInteractions();
    }

    @Test
    public void testDoFilterWhenJohnsoned() throws Exception {
        when(eventContainer.hasEvents()).thenReturn(true);

        filter.doFilter(request, response, filterChain);

        InOrder ordered = inOrder(config, eventContainer, filterChain, servletContext);
        ordered.verify(servletContext).getAttribute(eq(Johnson.ATTR_EVENT_CONTAINER));
        ordered.verify(eventContainer).hasEvents();
        ordered.verify(filterChain).doFilter(same(request), same(response));
        ordered.verifyNoMoreInteractions();
    }
}