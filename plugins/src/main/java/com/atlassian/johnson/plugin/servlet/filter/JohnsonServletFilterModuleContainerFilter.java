package com.atlassian.johnson.plugin.servlet.filter;

import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.plugin.servlet.filter.ServletFilterModuleContainerFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * Extends the plugin framework's {@code ServletFilterModuleContainerFilter} to bypass servlet-provided filters when
 * Johnson {@link com.atlassian.johnson.event.Event events} are present.
 * <p>
 * When the system is Johnsoned, the exact set of resources that are available is unknown. In some cases, that can
 * cause plugin-provided filters which attempt to access those resources to fail in an unexpected ways. That, in turn
 * can block access to Johnson's error page, or even hang request threads. Replacing the standard filter with this
 * Johnson-aware version ensures servlet-provided filters are automatically bypassed while Johnson events are present.
 *
 * @since 3.0
 */
public class JohnsonServletFilterModuleContainerFilter extends ServletFilterModuleContainerFilter {

    private static final Logger log = LoggerFactory.getLogger(JohnsonServletFilterModuleContainerFilter.class);

    /**
     * Checks whether plugin-provided filters should be {@link #bypassFilters() bypassed} and, if so, directly
     * invokes the {@code FilterChain}; otherwise, processes {@code super.doFilter} normally.
     *
     * @param request  the request
     * @param response the response
     * @param chain    the remaining filter chain
     * @throws IOException      if thrown by the base class, or the {@code FilterChain}
     * @throws ServletException if thrown by the base class, or the {@code FilterChain}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (bypassFilters()) {
            //If there are Johnson events, bypass plugin filters. This ensures those filters don't attempt
            //to interact with system resources which are not ready
            log.debug("{}: Bypassing plugin-provided filters; the system is locked", getFilterLocation());
            chain.doFilter(request, response);
        } else {
            //Otherwise, if there are no events, dispatch through servlet filters if they are available
            super.doFilter(request, response, chain);
        }
    }

    /**
     * Determines whether plugin-provided filters should be bypassed. The default implementation bypasses plugin-
     * provided filters whenever there are events in the {@link #getEventContainer() container}.
     * <p>
     * Applications can override this method in subclasses to apply a more specific set of restrictions. For example,
     * they could choose to allow plugin-provided filters to be applied in specific Johnson states, or based on other
     * knowledge of the application as a whole.
     *
     * @return {@code true} if plugin-provided filters should be bypassed; otherwise, {@code false} to apply
     *         plugin-provided filters normally
     */
    protected boolean bypassFilters() {
        return getEventContainer().hasEvents();
    }

    /**
     * Retrieves the {@link JohnsonEventContainer} using the {@code FilterConfig}'s {@code ServletContext}.
     *
     * @return the {@link JohnsonEventContainer}
     */
    protected JohnsonEventContainer getEventContainer() {
        return Johnson.getEventContainer(getFilterConfig().getServletContext());
    }
}
