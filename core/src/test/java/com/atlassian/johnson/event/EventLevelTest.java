package com.atlassian.johnson.event;

import com.atlassian.johnson.Johnson;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EventLevelTest {

    @BeforeClass
    public static void initialize() {
        Johnson.initialize("test-johnson-config.xml");
    }

    @AfterClass
    public static void terminate() {
        Johnson.terminate();
    }

    @Test
    public void testEventLevel() {
        EventLevel level = new EventLevel("foo", "bar");
        assertEquals("foo", level.getLevel());
        assertEquals("bar", level.getDescription());
    }

    @Test
    public void testGetEventLevel() {
        EventLevel expectedWarning = new EventLevel("warning", "This is a warning buddy");
        assertEquals(expectedWarning, EventLevel.get("warning"));
    }
}
