package com.atlassian.johnson;

import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventType;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultJohnsonEventContainerTest {

    private static final Predicate<Event> MATCH_NONE = event -> false;
    private static final Predicate<Event> MATCH_ALL = event -> true;

    private DefaultJohnsonEventContainer container;

    @Before
    public void setUp() {
        container = new DefaultJohnsonEventContainer();
    }

    @Test
    public void testContainerShouldAddAndRemoveEvents() {
        // no events at the start!
        assertFalse(container.hasEvents());

        // add an event and check it exists
        Event event = new Event(new EventType("systemic", "Systemic Anomaly"), "There is an anomaly in the matrix");
        container.addEvent(event);
        assertTrue(container.hasEvents());
        Collection<Event> containerEvents = container.getEvents();
        assertEquals(1, containerEvents.size());
        assertTrue(containerEvents.contains(event));

        // now remove the event and check it's gone
        container.removeEvent(event);
        assertFalse(container.hasEvents());
        assertEquals(0, container.getEvents().size());
    }

    @Test
    public void testEventsReturnedInOrderOfAddition() {
        for (int i = 0; i < 5; i++) {
            container.addEvent(new Event(new EventType("", ""), Integer.toString(i)));
        }

        assertTrue(container.hasEvents());
        assertThat(container.getEvents(), contains(eventsWithDescriptions("0", "1", "2", "3", "4")));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testReturnedCollectionOfEventsShouldNotBeModifiable() {
        container.getEvents().add(new Event(new EventType("", ""), ""));
    }

    @Test
    public void testGetEventsMatchingPredicateShouldApplyThatPredicate() {
        Event eventWithProgress = eventWithProgress(true);
        container.addEvent(eventWithProgress);
        container.addEvent(eventWithProgress(false));

        Collection<Event> returnedEvents = container.getEvents(Event::hasProgress);

        assertThat(returnedEvents, contains(eventWithProgress));
    }

    @Test
    public void testHasEventShouldReturnFalseWhenNoEventsMatchThePredicate() {
        container.addEvent(mock(Event.class));

        boolean hasEvent = container.hasEvent(MATCH_NONE);

        assertFalse(hasEvent);
    }

    @Test
    public void testHasEventShouldReturnTrueWhenAnEventMatchesThePredicate() {
        // Set up
        container.addEvent(mock(Event.class));

        // Invoke
        boolean hasEvent = container.hasEvent(MATCH_ALL);

        // Check
        assertTrue(hasEvent);
    }

    @Test
    public void testFirstEventShouldBeEmptyWhenContainerIsEmpty() {
        // Invoke
        Optional<Event> maybeEvent = container.firstEvent(MATCH_ALL);

        // Check
        assertThat(maybeEvent, is(empty()));
    }

    @Test
    public void testFirstEventShouldBeNonEmptyWhenContainerIsNotEmpty() {
        // Set up
        Event event = mock(Event.class);
        container.addEvent(event);

        // Invoke
        Optional<Event> maybeEvent = container.firstEvent(MATCH_ALL);

        // Check
        assertThat(maybeEvent, is(Optional.of(event)));
    }

    @Test
    public void testFirstEventShouldBeActualFirstEventWhenMultipleEventsArePresent() {
        // Set up
        Event event1 = mock(Event.class);
        container.addEvent(event1);
        container.addEvent(mock(Event.class));

        // Invoke
        Optional<Event> maybeEvent = container.firstEvent(MATCH_ALL);

        // Check
        assertThat(maybeEvent, is(Optional.of(event1)));
    }

    @Test
    public void testThatClearingNonEmptyContainerGivesAnEmptyContainer() {
        // Set up
        container.addEvent(mock(Event.class));

        // Invoke
        container.clear();

        // Check
        assertThat(container.getEvents(), is(emptyList()));
    }

    @Nonnull
    private static List<Matcher<? super Event>> eventsWithDescriptions(String... descriptions) {
        return stream(descriptions)
                .map(DefaultJohnsonEventContainerTest::eventWithDescription)
                .collect(toList());
    }

    @Nonnull
    private static Matcher<? super Event> eventWithDescription(String description) {
        return new FeatureMatcher<Event, String>(equalTo(description), "an Event with description", "description") {

            @Override
            protected String featureValueOf(Event actual) {
                return actual.getDesc();
            }
        };
    }

    @Nonnull
    private static Event eventWithProgress(boolean hasProgress) {
        Event event = mock(Event.class);
        when(event.hasProgress()).thenReturn(hasProgress);

        return event;
    }
}
