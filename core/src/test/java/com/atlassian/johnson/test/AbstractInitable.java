package com.atlassian.johnson.test;

import com.atlassian.johnson.Initable;

import javax.annotation.Nonnull;
import java.util.Map;

public abstract class AbstractInitable implements Initable {

    public void init(@Nonnull Map<String, String> params) {
    }
}
