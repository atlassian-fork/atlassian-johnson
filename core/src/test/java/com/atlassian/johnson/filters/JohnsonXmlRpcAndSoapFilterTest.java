package com.atlassian.johnson.filters;

import com.atlassian.johnson.DefaultJohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import org.junit.Test;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Tests both the JohnsonSoapFilter and JohnsonXmlRpcFilter classes.
 */
public class JohnsonXmlRpcAndSoapFilterTest {

    private static final String ERROR_MSG = "TEST ERROR MESSAGE";
    private static final String NOT_SETUP_ERROR_MESG = "The application has not yet been setup.";

    @Test
    public void testHandleErrorSoapJohnsonFilter() throws IOException {
        JohnsonSoapFilter johnsonSoapFilter = new JohnsonSoapFilter() {
            @Override
            protected String buildSoapFault(String errorMessage) {
                return getSoapFaultMessage(ERROR_MSG);
            }

            @Override
            protected String getStringForEvents(Collection<Event> events) {
                return null;
            }
        };
        testHandleError(johnsonSoapFilter, getSoapFaultMessage(ERROR_MSG));
    }

    @Test
    public void testHandleErrorXmlRpcJohnsonFilter() throws IOException {
        JohnsonXmlRpcFilter johnsonXmlRpcFilter = new JohnsonXmlRpcFilter() {
            @Override
            protected String buildXmlRpcErrorMessage(String error, int faultCode) {
                return getXmlRpcFaultMessage(ERROR_MSG);
            }

            @Override
            protected String getStringForEvents(Collection<Event> events) {
                return null;
            }
        };
        testHandleError(johnsonXmlRpcFilter, getXmlRpcFaultMessage(ERROR_MSG));
    }

    @Test
    public void testBuildXmlRpcErrorMessage() {
        JohnsonXmlRpcFilter filter = new JohnsonXmlRpcFilter();
        assertEquals(getXmlRpcFaultMessage(ERROR_MSG),
                filter.buildXmlRpcErrorMessage(ERROR_MSG, JohnsonXmlRpcFilter.FAULT_CODE));
    }

    @Test
    public void testHandleNotSetupXmlRpcJohnsonFilter() throws IOException {
        JohnsonXmlRpcFilter filter = new JohnsonXmlRpcFilter();
        testHandleNotSetup(filter, getXmlRpcFaultMessage(NOT_SETUP_ERROR_MESG));
    }

    @Test
    public void testHandleNotSetupSoapJohnsonFilter() throws IOException {
        JohnsonSoapFilter filter = new JohnsonSoapFilter();
        testHandleNotSetup(filter, getSoapFaultMessage(NOT_SETUP_ERROR_MESG));
    }

    private String getSoapFaultMessage(String notSetupErrorMesg) {
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n" +
                "                  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
                "    <soapenv:Body>\n" +
                "        <soapenv:Fault>\n" +
                "            <faultcode>soapenv:Server</faultcode>\n" +
                "            <faultstring>" +
                notSetupErrorMesg +
                "            </faultstring>\n" +
                "        </soapenv:Fault>\n" +
                "    </soapenv:Body>\n" +
                "</soapenv:Envelope>";
    }

    private String getXmlRpcFaultMessage(String errorMessage) {
        return "<?xml version=\"1.0\"?>\n" +

                "<methodResponse>\n" +
                "    <fault>\n" +
                "        <value>\n" +
                "            <struct>\n" +
                "                <member>\n" +
                "                    <name>faultString</name>\n" +
                "                    <value>" + errorMessage + "</value>\n" +
                "                </member>\n" +
                "                <member>\n" +
                "                    <name>faultCode</name>\n" +
                "                    <value>\n" +
                "                        <int>" + JohnsonXmlRpcFilter.FAULT_CODE + "</int>\n" +
                "                    </value>\n" +
                "                </member>\n" +
                "            </struct>\n" +
                "        </value>\n" +
                "    </fault>\n" +
                "</methodResponse>";
    }

    private void testHandleError(AbstractJohnsonFilter filter, String message) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream(1024);
        PrintWriter writer = new PrintWriter(stream);

        HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(writer);

        filter.handleError(new DefaultJohnsonEventContainer() {

            @Nonnull
            public List<Event> getEvents() {
                return Collections.emptyList();
            }
        }, null, response);

        verify(response).setContentType(eq(AbstractJohnsonFilter.TEXT_XML_UTF8_CONTENT_TYPE));
        verify(response).setStatus(eq(HttpServletResponse.SC_SERVICE_UNAVAILABLE));
        verify(response).getWriter();

        writer.flush();
        assertEquals(message, stream.toString());
    }

    private void testHandleNotSetup(AbstractJohnsonFilter filter, String message) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream(512);
        PrintWriter writer = new PrintWriter(stream);

        HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(writer);

        filter.handleNotSetup(null, response);

        verify(response).setContentType(eq(AbstractJohnsonFilter.TEXT_XML_UTF8_CONTENT_TYPE));
        verify(response).setStatus(eq(HttpServletResponse.SC_SERVICE_UNAVAILABLE));
        verify(response).getWriter();

        writer.flush();
        assertEquals(message, stream.toString());
    }
}
