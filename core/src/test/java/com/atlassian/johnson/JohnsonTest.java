package com.atlassian.johnson;

import org.junit.Test;

public class JohnsonTest {

    @Test(expected = IllegalStateException.class)
    public void testGetConfigThrowsWhenUninitialised() {
        Johnson.getConfig();
    }

    @Test(expected = IllegalStateException.class)
    public void testGetEventContainerThrowsWhenUninitialised() {
        Johnson.getEventContainer();
    }
}
