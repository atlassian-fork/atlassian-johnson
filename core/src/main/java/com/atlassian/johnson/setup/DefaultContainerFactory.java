package com.atlassian.johnson.setup;

import com.atlassian.johnson.DefaultJohnsonEventContainer;
import com.atlassian.johnson.JohnsonEventContainer;

import javax.annotation.Nonnull;

/**
 * This provides the old, non-multitenant functionality. This way existing apps don't need to worry about the
 * container-factory xml element and all that stuff
 *
 * @since 1.1
 */
public class DefaultContainerFactory implements ContainerFactory {

    @Nonnull
    public JohnsonEventContainer create() {
        return new DefaultJohnsonEventContainer();
    }
}
