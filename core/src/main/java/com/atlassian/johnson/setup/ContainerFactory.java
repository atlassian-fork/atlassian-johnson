package com.atlassian.johnson.setup;

import com.atlassian.johnson.JohnsonEventContainer;

import javax.annotation.Nonnull;

/**
 * Allows applications using Johnson to provide custom {@link JohnsonEventContainer} implementations for use as part
 * of their configuration.
 * <p>
 * For example, multi-tenant applications need a way to group events in the container per-tenant, allowing tenants to
 * see only those application and request events which relate to their view of the system. In such a scenario, the
 * application may register a custom {@code ContainerFactory} which returns a tenant-aware container. This keeps any
 * dependency on or knowledge of multi-tenancy libraries Johnson.
 *
 * @since 1.1
 */
public interface ContainerFactory {

    /**
     * Creates a {@link JohnsonEventContainer} for use storing events.
     * <p>
     * This method will be called <i>exactly once</i> during Johnson initialisation, and the returned event container
     * will be used to service all requests on all threads. As a result, the returned implementation is required to be
     * thread-safe. For multi-tenant systems, handling the differentiation between events for each tenant must happen
     * below the interface, on a single instance (at least from Johnson's perspective) of the container.
     *
     * @return an event container
     */
    @Nonnull
    JohnsonEventContainer create();
}
