package com.atlassian.johnson.setup;

import javax.annotation.Nonnull;

/**
 * Allows Johnson to interrogate the application to determine whether it has been setup, and whether a given URI relates
 * to its setup process (for those applications which offer web-based configuration).
 */
public interface SetupConfig {

    /**
     * Retrieves a flag indicating whether the application has been setup.
     *
     * @return {@code true} if the application has been setup; otherwise, {@code false}
     */
    boolean isSetup();

    /**
     * Retrieves a flag indicating whether the provided {@code uri} is a setup-related page.
     * <p>
     * Applications which require complex setup with multiple pages can implement this method to allow access to all
     * of their pages when {@link #isSetup()} returns {@code false}.
     * <p>
     * Note: This method is related, but not identical, to
     * {@link com.atlassian.johnson.config.JohnsonConfig#getSetupPath() JohnsonConfig.getSetupPath()}. That property
     * provides the URL to which a user should be redirected when the application is not setup. It defines the entry
     * point for setup. After the initial redirection, this method is used to determine whether a page being accessed
     * is part of the setup process.
     *
     * @param uri the URI of a web page
     * @return {@code true} if the URI references a setup page; otherwise, {@code false}
     */
    boolean isSetupPage(@Nonnull String uri);
}
