package com.atlassian.johnson.event;

import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;

/**
 * Predicates relating to Johnson events.
 *
 * @since 3.2
 */
@ParametersAreNonnullByDefault
public final class EventPredicates {

    private EventPredicates() {
        throw new UnsupportedOperationException(getClass().getSimpleName() + " should not be instantiated");
    }

    /**
     * Matches events which have the specified value for a given attribute. If the specified value is {@code null},
     * the predicate returns {@code true} iff the event has a {@code null} value for that attribute, which includes
     * not having any value for that attribute.
     *
     * @param name the name of the attribute to check
     * @param value the expected value, compared with {@code Object.equals}
     * @return a {@code Predicate} which returns {@code true} for {@link Event events} which have matching values
     *         for the specified attribute, and {@code false} for all others
     */
    @Nonnull
    public static Predicate<Event> attributeEquals(String name, @Nullable Object value) {
        requireNonNull(name, "name");

        return event -> Objects.equals(value, event.getAttribute(name));
    }

    /**
     * Matches events which have any of the specified {@link EventLevel levels}. If no levels are provided, the
     * predicate will always return {@code false}.
     *
     * @param levels the levels to compare against
     * @return a {@code Predicate} which returns {@code true} for {@link Event events} with one of the specified
     *         levels, and {@code false} for all others
     */
    @Nonnull
    public static Predicate<Event> level(EventLevel... levels) {
        requireNonNull(levels, "levels");
        if (levels.length == 0) {
            //Shortcut: If no levels were specified, Set.contains will never be true
            return event -> false;
        }

        Set<EventLevel> acceptedLevels = ImmutableSet.copyOf(levels);

        return event -> acceptedLevels.contains(event.getLevel());
    }

    /**
     * Matches events which have the specified {@link EventType type}.
     *
     * @param type the type to compare against
     * @return a {@code Predicate} which returns {@code true} for {@link Event events} whose {@link Event#getKey keys}
     *         match the provided {@link EventType}, and {@code false} for all others
     */
    @Nonnull
    public static Predicate<Event> type(EventType type) {
        requireNonNull(type, "type");

        return event -> type.equals(event.getKey());
    }
}
