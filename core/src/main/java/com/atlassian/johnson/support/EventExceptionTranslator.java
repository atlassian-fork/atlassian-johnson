package com.atlassian.johnson.support;

import com.atlassian.johnson.event.Event;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Describes a strategy for translating an exception to an {@link Event}.
 *
 * @since 3.0
 */
public interface EventExceptionTranslator {

    /**
     * Attempt to translate the provided {@code Throwable} to an {@link Event event}. Implementors may return
     * {@code null} if an event should not be created, or to allow for chaining translators to handle specific
     * cases separately.
     *
     * @param thrown the exception to translate
     * @return an {@link Event event}, or {@code null} if the exception could not, or should not, be translated
     */
    @Nullable
    Event translate(@Nonnull Throwable thrown);
}
