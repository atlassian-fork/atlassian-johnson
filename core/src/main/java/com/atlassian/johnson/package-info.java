/**
 * Johnson is an application consistency framework.
 * <p>
 * Johnson's primary interaction point is {@link com.atlassian.johnson.Johnson Johnson}, which provides methods to
 * initialize the framework and accessors for the {@link com.atlassian.johnson.config.JohnsonConfig configuration}
 * and {@link com.atlassian.johnson.JohnsonEventContainer event container} which are available after initialization.
 * <p>
 * The {@link com.atlassian.johnson.config config} package contains a standard configuration interface as well as a
 * default implementation which loads configuration from an XML file. Additional documentation on how to configure
 * Johnson is available in that package, including documentation on the default XML format.
 * <p>
 * The {@link com.atlassian.johnson.context context} package contains a {@code ServletContextListener} which can be
 * wired into a web application to initialise and terminate Johnson as part of the web application's lifecycle. This
 * is the preferred way to manage the framework's lifecycle.
 * <p>
 * The {@link com.atlassian.johnson.filters filters} package contains various servlet filters which can be wired into
 * a web application to achieve different behaviours, such as redirecting users to a standard error page or returning
 * a standard 503 Service Unavailable error. Further documentation is available in that package.
 */
package com.atlassian.johnson;