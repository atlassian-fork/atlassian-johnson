package com.atlassian.johnson.config;

import com.atlassian.johnson.Initable;
import com.atlassian.johnson.event.*;
import com.atlassian.johnson.setup.ContainerFactory;
import com.atlassian.johnson.setup.DefaultContainerFactory;
import com.atlassian.johnson.setup.DefaultSetupConfig;
import com.atlassian.johnson.setup.SetupConfig;
import com.atlassian.johnson.util.StringUtils;
import com.atlassian.plugin.servlet.util.DefaultPathMapper;
import com.atlassian.plugin.servlet.util.PathMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.opensymphony.util.ClassLoaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.annotation.Nonnull;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.URL;
import java.util.*;
import java.util.stream.StreamSupport;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

/**
 * Loads configuration for Johnson from an XML file.
 *
 * @since 2.0
 */
public class XmlJohnsonConfig implements JohnsonConfig {

    public static final String DEFAULT_CONFIGURATION_FILE = "johnson-config.xml";

    private static final Logger LOG = LoggerFactory.getLogger(XmlJohnsonConfig.class);

    private final List<ApplicationEventCheck> applicationEventChecks;
    private final ContainerFactory containerFactory;
    private final String errorPath;
    private final List<EventCheck> eventChecks;
    private final Map<Integer, EventCheck> eventChecksById;
    private final Map<String, EventLevel> eventLevels;
    private final Map<String, EventType> eventTypes;
    private final PathMapper ignoreMapper;
    private final List<String> ignorePaths;
    private final Map<String, String> params;
    private final List<RequestEventCheck> requestEventChecks;
    private final SetupConfig setupConfig;
    private final String setupPath;

    private XmlJohnsonConfig(SetupConfig setupConfig, ContainerFactory containerFactory, List<EventCheck> eventChecks,
                             Map<Integer, EventCheck> eventChecksById, Map<String, EventLevel> eventLevels,
                             Map<String, EventType> eventTypes, List<String> ignorePaths, Map<String, String> params,
                             String setupPath, String errorPath) {
        this.containerFactory = containerFactory;
        this.errorPath = errorPath;
        this.eventChecks = eventChecks;
        this.eventChecksById = eventChecksById;
        this.eventLevels = eventLevels;
        this.eventTypes = eventTypes;
        this.ignorePaths = ignorePaths;
        this.params = params;
        this.setupConfig = setupConfig;
        this.setupPath = setupPath;

        ImmutableList.Builder<ApplicationEventCheck> applicationBuilder = ImmutableList.builder();
        ImmutableList.Builder<RequestEventCheck> requestBuilder = ImmutableList.builder();
        for (EventCheck eventCheck : eventChecks) {
            if (eventCheck instanceof ApplicationEventCheck) {
                applicationBuilder.add((ApplicationEventCheck) eventCheck);
            }
            if (eventCheck instanceof RequestEventCheck) {
                requestBuilder.add((RequestEventCheck) eventCheck);
            }
        }
        applicationEventChecks = applicationBuilder.build();
        requestEventChecks = requestBuilder.build();

        ignoreMapper = new DefaultPathMapper();
        ignoreMapper.put(errorPath, errorPath);
        ignoreMapper.put(setupPath, setupPath);
        for (String path : ignorePaths) {
            ignoreMapper.put(path, path);
        }
    }

    @Nonnull
    public static XmlJohnsonConfig fromDocument(@Nonnull Document document) {
        Element root = checkNotNull(document, "document").getDocumentElement();

        SetupConfig setupConfig = configureClass(root, "setup-config", SetupConfig.class, DefaultSetupConfig.class);
        ContainerFactory containerFactory = configureClass(root, "container-factory",
                ContainerFactory.class, DefaultContainerFactory.class);
        Map<String, EventLevel> eventLevels = configureEventConstants(root, "event-levels", EventLevel.class);
        Map<String, EventType> eventTypes = configureEventConstants(root, "event-types", EventType.class);
        Map<String, String> params = configureParameters(root);
        String setupPath = Iterables.getOnlyElement(configurePaths(root, "setup"));
        String errorPath = Iterables.getOnlyElement(configurePaths(root, "error"));
        List<String> ignorePaths = configurePaths(root, "ignore");

        ElementIterable elements = getElementsByTagName(root, "event-checks");

        ArrayList<EventCheck> checks = new ArrayList<>(elements.size());
        Map<Integer, EventCheck> checksById = new HashMap<>(elements.size());
        if (!elements.isEmpty()) {
            elements = getElementsByTagName(Iterables.getOnlyElement(elements), "event-check");
            for (Element element : elements) {
                EventCheck check = parseEventCheck(element);
                checks.add(check);

                String id = element.getAttribute("id");
                if (!StringUtils.isBlank(id)) {
                    try {
                        if (checksById.put(Integer.parseInt(id), check) != null) {
                            throw new ConfigurationJohnsonException("EventCheck ID [" + id + "] is not unique");
                        }
                    } catch (NumberFormatException e) {
                        throw new ConfigurationJohnsonException("EventCheck ID [" + id + "] is not a number", e);
                    }
                }
            }
        }

        return new XmlJohnsonConfig(setupConfig, containerFactory, ImmutableList.copyOf(checks),
                ImmutableMap.copyOf(checksById), eventLevels, eventTypes, ignorePaths, params, setupPath, errorPath);
    }

    @Nonnull
    public static XmlJohnsonConfig fromFile(@Nonnull String fileName) {
        URL url = ClassLoaderUtil.getResource(checkNotNull(fileName, "fileName"), XmlJohnsonConfig.class);
        if (url != null) {
            LOG.debug("Loading {} from classpath at {}", fileName, url);
            fileName = url.toString();
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(fileName);

            return fromDocument(document);
        } catch (IOException e) {
            throw new ConfigurationJohnsonException("Failed to parse [" + fileName + "]; the file could not be read", e);
        } catch (ParserConfigurationException e) {
            throw new ConfigurationJohnsonException("Failed to parse [" + fileName + "]; JVM configuration is invalid", e);
        } catch (SAXException e) {
            throw new ConfigurationJohnsonException("Failed to parse [" + fileName + "]; XML is not well-formed", e);
        }
    }

    @Nonnull
    public List<ApplicationEventCheck> getApplicationEventChecks() {
        return applicationEventChecks;
    }

    @Nonnull
    public ContainerFactory getContainerFactory() {
        return containerFactory;
    }

    @Nonnull
    public String getErrorPath() {
        return errorPath;
    }

    public EventCheck getEventCheck(int id) {
        return eventChecksById.get(id);
    }

    @Nonnull
    public List<EventCheck> getEventChecks() {
        return eventChecks;
    }

    public EventLevel getEventLevel(@Nonnull String level) {
        return eventLevels.get(checkNotNull(level, "level"));
    }

    public EventType getEventType(@Nonnull String type) {
        return eventTypes.get(checkNotNull(type, "type"));
    }

    @Nonnull
    public List<String> getIgnorePaths() {
        return ignorePaths;
    }

    @Nonnull
    public Map<String, String> getParams() {
        return params;
    }

    @Nonnull
    public List<RequestEventCheck> getRequestEventChecks() {
        return requestEventChecks;
    }

    @Nonnull
    public SetupConfig getSetupConfig() {
        return setupConfig;
    }

    @Nonnull
    public String getSetupPath() {
        return setupPath;
    }

    public boolean isIgnoredPath(@Nonnull String uri) {
        return ignoreMapper.get(checkNotNull(uri, "uri")) != null;
    }

    private static <T> Map<String, T> configureEventConstants(Element root, String tagName, Class<T> childClass) {
        Constructor<T> constructor;
        try {
            constructor = childClass.getConstructor(String.class, String.class);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("Class [" + childClass.getName() +
                    "] requires a String, String constructor");
        }

        ElementIterable elements = getElementsByTagName(root, tagName);
        if (elements.isEmpty()) {
            return Collections.emptyMap();
        }
        elements = getElementsByTagName(Iterables.getOnlyElement(elements), tagName.substring(0, tagName.length() - 1));

        ImmutableMap.Builder<String, T> builder = ImmutableMap.builder();
        for (Element element : elements) {
            String key = element.getAttribute("key");
            String description = getContainedText(element, "description");

            try {
                builder.put(key, constructor.newInstance(key, description));
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException("Constructor [" + constructor.getName() + "] must be public");
            } catch (InstantiationException e) {
                throw new IllegalArgumentException("Class [" + childClass.getName() + "] may not be abstract");
            } catch (InvocationTargetException e) {
                Throwable cause = e.getCause();
                if (cause instanceof RuntimeException) {
                    throw (RuntimeException) cause;
                }
                throw new UndeclaredThrowableException(cause);
            }
        }
        return builder.build();
    }

    private static List<String> configurePaths(Element root, String tagname) {
        ElementIterable elements = getElementsByTagName(root, tagname);
        if (elements.isEmpty()) {
            return Collections.emptyList();
        }
        elements = getElementsByTagName(Iterables.getOnlyElement(elements), "path");

        return StreamSupport.stream(elements.spliterator(), false)
                .map(element -> ((Text) element.getFirstChild()))
                .map(Text::getData)
                .map(String::trim)
                .collect(collectingAndThen(toList(), ImmutableList::copyOf));
    }

    private static Map<String, String> configureParameters(Element root) {
        NodeList list = root.getElementsByTagName("parameters");
        if (isEmpty(list)) {
            return Collections.emptyMap();
        }

        Element element = (Element) list.item(0);
        return getInitParameters(element);
    }

    @Nonnull
    private static <T> T configureClass(Element root, String tagname, Class<T> expectedClass, Class<? extends T> defaultClass) {
        ElementIterable elements = getElementsByTagName(root, tagname);
        if (elements.isEmpty()) {
            try {
                return defaultClass.newInstance();
            } catch (Exception e) {
                throw new ConfigurationJohnsonException("Default [" + expectedClass.getName() + "], [" +
                        defaultClass.getName() + "] is not valid", e);
            }
        }

        Element element = Iterables.getOnlyElement(elements);
        String className = element.getAttribute("class");
        try {
            Class<?> clazz = ClassLoaderUtil.loadClass(className, XmlJohnsonConfig.class);
            if (!expectedClass.isAssignableFrom(clazz)) {
                throw new ConfigurationJohnsonException("The class specified by " + tagname + " (" + className +
                        ") is required to implement [" + expectedClass.getName() + "]");
            }

            T instance = expectedClass.cast(clazz.newInstance());
            if (instance instanceof Initable) {
                Map<String, String> params = getInitParameters(element);
                ((Initable) instance).init(params);
            }
            return instance;
        } catch (Exception e) {
            throw new ConfigurationJohnsonException("Could not create: " + tagname, e);
        }
    }

    private static Map<String, String> getInitParameters(Element root) {
        ElementIterable elements = new ElementIterable(root.getElementsByTagName("init-param"));

        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        for (Element element : elements) {
            String paramName = getContainedText(element, "param-name");
            String paramValue = getContainedText(element, "param-value");
            builder.put(paramName, paramValue);
        }
        return builder.build();
    }

    private static String getContainedText(Node parent, String childTagName) {
        try {
            Node tag = ((Element) parent).getElementsByTagName(childTagName).item(0);
            return ((Text) tag.getFirstChild()).getData();
        } catch (Exception e) {
            return null;
        }
    }

    private static ElementIterable getElementsByTagName(Node parent, String tagName) {
        Element element = (Element) parent;
        NodeList list = element.getElementsByTagName(tagName);
        if (isEmpty(list) && tagName.contains("-")) {
            //Many tags used to not have hyphens, so we fall back to the old run-together approach
            list = element.getElementsByTagName(tagName.replace("-", ""));
        }

        return new ElementIterable(list);
    }

    private static boolean isEmpty(NodeList list) {
        return (list == null || list.getLength() == 0);
    }

    private static EventCheck parseEventCheck(Element element) {
        String className = element.getAttribute("class");
        if (StringUtils.isBlank(className)) {
            throw new ConfigurationJohnsonException("event-check element with bad class attribute");
        }

        Object o;
        try {
            LOG.trace("Loading class [{}]", className);
            Class eventCheckClazz = ClassLoaderUtil.loadClass(className, XmlJohnsonConfig.class);
            LOG.trace("Instantiating [{}]", className);
            o = eventCheckClazz.newInstance();
        } catch (ClassNotFoundException e) {
            LOG.error("Failed to load EventCheck class [" + className + "]", e);
            throw new ConfigurationJohnsonException("Could not load EventCheck: " + className, e);
        } catch (IllegalAccessException e) {
            LOG.error("Missing public nullary constructor for EventCheck class [" + className + "]", e);
            throw new ConfigurationJohnsonException("Could not instantiate EventCheck: " + className, e);
        } catch (InstantiationException e) {
            LOG.error("Could not instantiate EventCheck class [" + className + "]", e);
            throw new ConfigurationJohnsonException("Could not instantiate EventCheck: " + className, e);
        }

        if (!(o instanceof EventCheck)) {
            throw new ConfigurationJohnsonException(className + " does not implement EventCheck");
        }

        LOG.debug("Adding EventCheck of class: " + className);
        EventCheck eventCheck = (EventCheck) o;
        if (eventCheck instanceof Initable) {
            ((Initable) eventCheck).init(getInitParameters(element));
        }
        return eventCheck;
    }

    private static class ElementIterable implements Iterable<Element> {

        private final NodeList list;

        private ElementIterable(NodeList list) {
            this.list = list;
        }

        @Nonnull
        @Override
        public Iterator<Element> iterator() {
            return new Iterator<Element>() {

                private int index;

                public boolean hasNext() {
                    return index < list.getLength();
                }

                public Element next() {
                    if (hasNext()) {
                        return (Element) list.item(index++);
                    }
                    throw new NoSuchElementException();
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }

        public boolean isEmpty() {
            return (list == null || list.getLength() == 0);
        }

        public int size() {
            return (list == null ? 0 : list.getLength());
        }
    }
}
