package com.atlassian.johnson.config;

import com.atlassian.johnson.event.*;
import com.atlassian.johnson.setup.ContainerFactory;
import com.atlassian.johnson.setup.SetupConfig;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

/**
 * @since 2.0
 */
public interface JohnsonConfig {

    @Nonnull
    List<ApplicationEventCheck> getApplicationEventChecks();

    @Nonnull
    ContainerFactory getContainerFactory();

    @Nonnull
    String getErrorPath();

    @Nullable
    EventCheck getEventCheck(int id);

    @Nonnull
    List<EventCheck> getEventChecks();

    @Nullable
    EventLevel getEventLevel(@Nonnull String level);

    @Nullable
    EventType getEventType(@Nonnull String type);

    @Nonnull
    List<String> getIgnorePaths();

    @Nonnull
    Map<String, String> getParams();

    @Nonnull
    List<RequestEventCheck> getRequestEventChecks();

    @Nonnull
    SetupConfig getSetupConfig();

    @Nonnull
    String getSetupPath();

    boolean isIgnoredPath(@Nonnull String uri);
}
